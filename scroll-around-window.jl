;; Copyright (c) 2012 Michele Bini

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the version 3 of the GNU General Public License
;; as published by the Free Software Foundation.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'viewport-scrolling)

(defvar scroll-around-window--timer nil)

(defun scroll-around-window ()
  (when viewport-scrolling-done
    (remove-hook 'viewport-scrolling-done
		 scroll-around-window))
  (when scroll-around-window--timer
    (delete-timer scroll-around-window--timer)
    (setq scroll-around-window--timer nil))
  (let ((w (input-focus)))
    (when w
      (let ((d (window-frame-dimensions w))
	    (sw (screen-width))
	    (sh (screen-height)))
	(when (or (> (car d) sw) (> (cdr d) sh))
	  (let ((x (window-position w))
		(sw (screen-width))
		(sh (screen-height)))
	    (let* ((p (query-pointer t))
		   (rx (+ viewport-x-offset (* sw -0.5) (min (+ (car x) (car d)) (max (car x) (car p)))))
		   (ry (+ viewport-y-offset (* sh -0.5) (min (+ (cdr x) (cdr d)) (max (cdr x) (cdr p))))))
	      (viewport-scroll-to rx ry)))
	  (setq scroll-around-window--timer
		(make-timer scroll-around-window
			    0 1000)))))))

(defun scroll-around-window-1 ()
  (if (viewport-scrolling-p)
      (add-hook 'viewport-scrolling-done scroll-around-window)
    (scroll-around-window)))

(defun turn-on-scroll-around-window ()
  (add-hook 'focus-in-hook        scroll-around-window-1)
  (add-hook 'unshade-window-hook  scroll-around-window-1)
  (add-hook 'after-resize-hook    scroll-around-window-1)
  (scroll-around-window-1))

(defun turn-off-scroll-around-window ()
  (remove-hook 'focus-in-hook        scroll-around-window-1)
  (remove-hook 'unshade-window-hook  scroll-around-window-1)
  (remove-hook 'after-resize-hook    scroll-around-window-1))

(provide 'scroll-around-window)