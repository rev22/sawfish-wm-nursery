;; Copyright (c) 2002, 2012 Michele Bini

;; This is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public
;; License along with this program; if not, write to the Free
;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
;; MA 02111-1307 USA

(require 'viewport-scrolling)
(require 'rep.io.timers)

(defvar scroll-into-strategy 'center)

(defun scroll-into-window ()
  "Adjusts the viewport making the focused window fully visible."
  (let ((window (input-focus)))
    (when window
      (unless (window-get window 'sticky-viewport)
	(let* ((ix 0) (iy 0)
	       (p (window-position window))
	       (d (window-frame-dimensions window))
	       (sw (screen-width))
	       (sh (screen-height))
	       (w1 (car p))
	       (we (car d))
	       (w2 (+ w1 we))
	       (h1 (cdr p))
	       (he (cdr d))
	       (h2 (+ h1 he)))
	  (case scroll-into-strategy
	    ((marginal)
	     (when (<= (car d) sw)
	       (if (< w1 0) (setq ix w1)
		 (if (> w2 sw) (setq ix (- w2 sw)))))
	     (when (<= (cdr d) sh)
	       (if (< h1 0) (setq iy h1)
		 (if (> h2 sh) (setq iy (- h2 sh))))))
	    ((center)
	     (setq ix (- w1 (quotient (- sw we) 2))
		   iy (- h1 (quotient (- sh he) 2)))))
	  (viewport-scroll-to
	   (+ viewport-x-offset ix)
	   (+ viewport-y-offset iy)))))))

(defun scroll-into-window-offscreen-p (window)
  (let ((p (window-position window))
	(d (window-frame-dimensions window))
	(sw (screen-width))
	(sh (screen-height)))
    (or (< (car p) 0) (< (cdr p) 0)
	(> (+ (car p) (car d)) sw)
	(> (+ (cdr p) (cdr d)) sh))))

;; Return how much of a window is visible
(defun scroll-into-window--visibility (window)
  (let ((p (window-position window))
	(d (window-frame-dimensions window))
	(sw (screen-width))
	(sh (screen-height)))
    (* (/ (- (min sw (max 0 (+ (car p) (car d)))) (max (car p) 0)) (car d))
       (/ (- (min sh (max 0 (+ (cdr p) (cdr d)))) (max (cdr p) 0)) (cdr d)))))

;; (custom-declare-group 'scroll-into-window
;;   "Scroll into window."
;;   ':group 'workspace)

(defcustom scroll-into-window-delay 5000
  "Delay before scrolling to a newly selected window.
In milliseconds."
  :type number
  :group workspace)

(defcustom scroll-into-window-offscreen-delay 900
  "Delay before scrolling to a newly selected offscreen window.
In milliseconds."
  :type number
  :group workspace)

(defvar scroll-into-window-timer nil)
(defun scroll-into-window-with-delay ()
  (interactive)
  (let ((w (input-focus)))
    (when w
      (when (window-on-top-p w)
	(when scroll-into-window-timer
	  (delete-timer scroll-into-window-timer)
	  (setq scroll-into-window-timer nil))
	(setq scroll-into-window-timer
	      (make-timer scroll-into-window 0
			  (if (scroll-into-window-offscreen-p w)
			      (* (if (viewport-scrolling-p) 0.35 1)
				 scroll-into-window-offscreen-delay
				 (scroll-into-window--visibility w))
			    scroll-into-window-delay)))))))

(defun turn-on-scroll-into ()
  (add-hook 'focus-in-hook        scroll-into-window-with-delay)
  (add-hook 'unshade-window-hook  scroll-into-window-with-delay)
  (add-hook 'after-resize-hook    scroll-into-window-with-delay)
  (add-hook 'after-move-hook      scroll-into-window-with-delay)
  (scroll-into-window-with-delay))

(defun turn-off-scroll-into ()
  (remove-hook 'focus-in-hook        scroll-into-window-with-delay)
  (remove-hook 'unshade-window-hook  scroll-into-window-with-delay)
  (remove-hook 'after-resize-hook    scroll-into-window-with-delay)
  (remove-hook 'after-move-hook      scroll-into-window-with-delay))

(provide 'scroll-into)
