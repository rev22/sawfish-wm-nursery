;; Copyright (c) 2002, 2012 Michele Bini

;; This is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public
;; License along with this program; if not, write to the Free
;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
;; MA 02111-1307 USA

(require 'rep.io.timers)

;;(defgroup viewport-scrolling "Animated viewport scrolling support"
;;  :group workspace)

(defvar viewport-scrolling--goalx 0)
(defvar viewport-scrolling--goaly 0)
(defvar viewport-scrolling--x 0.0)
(defvar viewport-scrolling--y 0.0)
(defvar viewport-scrolling--x-speed 0.0)
(defvar viewport-scrolling--y-speed 0.0)
(defvar viewport-scrolling--max-acceleration 5)
(defvar viewport-scrolling--max-speed nil)
(defcustom viewport-scrolling--delay 120
  "Delay for the viewport scrolling animation, in milliseconds"
  :type number
  :group workspace)
(defvar viewport-scrolling--timer nil)
(defun set-viewport-preserving-pointer (x y)
  (let* ((b 10)
	 (ox viewport-x-offset)
	 (oy viewport-y-offset)
	 (mx (- (screen-width) b))
	 (my (- (screen-height) b))
	 (p (query-pointer t)))
    (set-viewport x y)
    (let ((nx (- (+ (car p) ox) x))
	  (ny (- (+ (cdr p) oy) y)))
      (if (< nx b) (setq nx b)
	(if (>= nx mx) (setq nx mx)))
      (if (< ny b) (setq ny b)
	(if (>= ny my) (setq ny my)))
      (warp-cursor nx ny))))

(setq viewport-scrolling-done nil)

(defun viewport-scrolling--run ()
  (let*
      ((ovx  viewport-scrolling--x-speed)
       (ovy  viewport-scrolling--y-speed)
       (ma   viewport-scrolling--max-acceleration)
       (vx   (- viewport-scrolling--goalx viewport-scrolling--x))
       (vy   (- viewport-scrolling--goaly viewport-scrolling--y))
       (r    (max 0.4 (round (/ (* (sqrt (+ (expt ovx 2) (expt ovy 2))) 2) ma))))
       (vx   (- vx (* r viewport-scrolling--x-speed)))
       (vy   (- vy (* r viewport-scrolling--y-speed)))
       (ms   viewport-scrolling--max-speed))
    (when ma
      (let ((ax (- vx ovx))
	    (ay (- vy ovy)))
	(let ((a (+ (expt ax 2) (expt ay 2))))
	  (when (> a (expt ma 2))
	    (setq a (/ ma (sqrt a)))
	    (setq ax (* ax a)
		  ay (* ay a))
	    (setq vx (+ ax ovx)
		  vy (+ ay ovy))))))
    (when ms
      (let ((s (+ (expt vx 2) (expt vy 2))))
	(when (> s (expt ms 2))
	  (setq s (/ ms (sqrt s)))
	  (setq vx (* vx s)
		vy (* vy s)))))
    (setq viewport-scrolling--x-speed vx)
    (setq viewport-scrolling--y-speed vy)
    (setq viewport-scrolling--x (+ viewport-scrolling--x vx))
    (setq viewport-scrolling--y (+ viewport-scrolling--y vy)))
  (set-viewport-preserving-pointer
   (inexact->exact (round viewport-scrolling--x))
   (inexact->exact (round viewport-scrolling--y)))
  (if (and (= viewport-scrolling--goalx viewport-x-offset)
	   (= viewport-scrolling--goaly viewport-y-offset))
      (progn
	(when viewport-scrolling--timer
	  (delete-timer viewport-scrolling--timer)
	  (setq viewport-scrolling--timer nil))
	(setq viewport-scrolling--x viewport-x-offset
	      viewport-scrolling--y viewport-y-offset
	      viewport-scrolling--x-speed 0.0
	      viewport-scrolling--y-speed 0.0)
	(call-hook 'viewport-scrolling-done))
    (setq viewport-scrolling--timer
	  (make-timer viewport-scrolling--run 0
		      viewport-scrolling--delay))))

(defun viewport-scrolling-p ()
  (if viewport-scrolling--timer t nil))

(defun viewport-scroll-to (x y)
  (setq viewport-scrolling--goalx (inexact->exact (round x))
	viewport-scrolling--goaly (inexact->exact (round y)))
  (unless viewport-scrolling--timer
    (setq viewport-scrolling--x viewport-x-offset)
    (setq viewport-scrolling--y viewport-y-offset)
    (viewport-scrolling--run)))

(defun scroll-to-pointer ()
  (let ((sw (screen-width))
	(sh (screen-height)))
    (let ((p (query-pointer t)))
      (viewport-scroll-to
       (+ (car p) (* sw -0.5) viewport-scrolling--x)
       (+ (cdr p) (* sh -0.5) viewport-scrolling--y)))))

(provide 'viewport-scrolling)
